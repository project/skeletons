CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * How to use
 * Full synchronisation
 * Maintainers


INTRODUCTION
------------
Current maintainer: laszlo.kovacs <laszlo.kovacs@cheppers.com>

This module provides a UI for developers to create module and install profile
skeletons. It van add a form skeleton to the module if the option is selected.
It is able to clone a selected theme as well.

After installation go to Configuration > Development > Skeletons
(admin/config/development/skeletons) tab.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/projects/skeletons

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/skeletons?categories=All


REQUIREMENTS
------------
No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled a 'Skeletons' menu will a appear on admin/config/development/skeletons
page.

HOW TO USE
----------------
After enabling a 'Skeletons' tab will appear on
admin/config/development/skeletons page. Click on the Moduler tab and fill in
the form. Click create and your module skeleton will be created on selected path.
Go to Extend menu (admin/modules) and enable it.
If you want to create an install profile skeleton select the Profiler tab.
Fill in the form and click Create. If you want to clone a theme click Add the
checkbox. Your profile skeleton will be created under selected path with included
theme if the option was selected.

MAINTAINERS
-----------
Current maintainers:
 * László Kovács - https://www.drupal.org/u/laszlo.kovacs

This project has been sponsored by:
 * Cheppers Ltd.
   A team of experienced web developers working with the Drupal content
   management system. Expertised in Drupal consulting, development, professional
   website design, search engine optimization (SEO), e-commerce and data
   migration.
   Visit https://cheppers.com/ for more information.
