<?php

/**
 * @file
 * Contains \Drupal\skeletons\Controller\SkeletonsController.
 */

namespace Drupal\skeletons\Controller;

use \Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for config module routes.
 */
class SkeletonsController extends ControllerBase{

  public function createModuleSkeletons($module_name) {
    $result = array(
      '#type' => 'markup',
      '#markup' => $this->t('Your %s module has been succesfully created.', $module_name),
    );
    return $result;
  }

}
