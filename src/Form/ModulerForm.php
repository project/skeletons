<?php

/**
 * @file
 * Contains \Drupal\skeletons\Form\ModulerForm.
 */

namespace Drupal\skeletons\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidator;
use Drupal\Core\File\FileSystem;

class ModulerForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  protected $profile;
  protected $profilePath;
  protected $destPathOptions;

  public function __construct() {
    $this->profile = drupal_get_profile();
    $this->profilePath = drupal_get_path('profile', $this->profile);
    $this->destPathOptions = array(
      'profiles/%my_profile%/modules/custom' => $this->profilePath . "/modules/custom",
      'modules/custom' => 'modules/custom',
      'other' => 'Other',
    );
  }

  public function getFormId() {
    return 'skeletons_moduler_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['module_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Module name'),
      '#description' => $this->t('Name of your module. Your module may not have
        the same short name as any other module, theme, or installation profile
        you will be using on the site.
        It may not be any of the reserved terms : src, lib, vendor, assets, css,
        files, images, js, misc, templates, includes, fixtures, Drupal.'),
      '#required' => TRUE,
    );
    $form['machine_name'] = array(
      '#type' => 'machine_name',
      '#default_value' => '',
      '#maxlength' => 64,
      '#description' => $this->t('A unique name for this item. It must only contain lowercase letters, numbers, and underscores.'),
      '#machine_name' => array(
        'exists' => array($this, 'exists'),
        'source' => array('module_name'),
      ),
    );
    $form['module_description'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Module description'),
      '#description' => $this->t('Description of the module.'),
      '#default_value' => 'My module description',
      '#required' => TRUE,
    );
    $form['module_dependencies'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Module dependencies'),
      '#description' => $this->t('Dependencies, separated by space or comma.'),
    );

    $form['module_dest_path'] = array(
      '#type' => 'radios',
      '#title' => 'Module destination path',
      '#options' => $this->destPathOptions,
      '#default_value' => 'profiles/%my_profile%/modules/custom',
    );
    $form['module_dest_path_other'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Other destination path'),
      '#description' => $this->t('Provide other destination path e.g /my/custom/path'),
      '#states' => array(
        'visible' => array(
          ':input[name="module_dest_path"]' => array('value' => 'other'),
        ),
        'required' => array(
          ':input[name="module_dest_path"]' => array('value' => 'other'),
        ),
      ),
    );
//    $form['module_export'] = array(
//      '#type' => 'submit',
//      '#value' => $this->t('Export'),
//      '#states' => array(
//        'visible' => array(
//          ':input[name="module_dest_path"]' => array('value' => 'export'),
//        ),
//        'required' => array(
//          ':input[name="module_dest_path"]' => array('value' => 'export'),
//        ),
//      ),
//    );
    $form['module_package'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Package'),
      '#description' => $this->t('Name of package.'),
      '#default_value' => drupal_get_profile(),
    );
    $form['add_form_skeletons'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Add form skeletons'),
      '#default_value' => FALSE,
    );
    $form['form_path'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Form path'),
      '#description' => $this->t('Add form path e.g. /myform.'),
      '#default_value' => '',
      '#states' => array(
        'visible' => array(
          ':input[name="add_form_skeletons"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="add_form_skeletons"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['description'] = array(
      '#markup' => '<p><b>' . $this->t('Use the Create button to create a skeletons of a new module.') . '</b></p>',
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Create'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $dest_path = '';
    if ($user_input['module_dest_path'] == 'other') {
      $dest_path = $user_input['module_dest_path_other'];
    }
    elseif ($user_input['module_dest_path'] == 'modules/custom') {
      $dest_path = $user_input['module_dest_path'];
    }
    elseif ($user_input['module_dest_path'] == 'profiles/%my_profile%/modules/custom') {
      $profile = drupal_get_profile();
      $dest_path = str_replace('%my_profile%', $profile, $user_input['module_dest_path']);
    }
    $dest_path .= "/{$user_input['machine_name']}";
    $filename = $dest_path . '/' . $user_input['machine_name'];

    $src_path = "$dest_path/src";
    $form_path = "$src_path/Form";
    $form_filename = "$form_path/{$this->makeClassName($user_input['machine_name'])}";
    if (DIRECTORY_SEPARATOR != '/') {
      $dest_path = str_replace('/', DIRECTORY_SEPARATOR, $dest_path);
      $src_path = str_replace('/', DIRECTORY_SEPARATOR, $src_path);
      $form_path = str_replace('/', DIRECTORY_SEPARATOR, $form_path);
      $filename = str_replace('/', DIRECTORY_SEPARATOR, $filename);
      $form_filename = str_replace('/', DIRECTORY_SEPARATOR, $form_filename);
    }
    \Drupal::service('file_system')->mkdir($dest_path, FileSystem::CHMOD_DIRECTORY, TRUE);

    $content = $this->makeInfoFileContent($user_input);
    file_put_contents($filename . '.info.yml', $content);
    $content = $this->makeInstallFileContent($user_input);
    file_put_contents($filename . '.install', $content);
    $content = $this->makeModuleFileContent($user_input);
    file_put_contents($filename . '.module', $content);
    if ($user_input['add_form_skeletons']) {
      \Drupal::service('file_system')->mkdir($src_path, FileSystem::CHMOD_DIRECTORY, TRUE);
      \Drupal::service('file_system')->mkdir($form_path, FileSystem::CHMOD_DIRECTORY, TRUE);
      $content = $this->makeFormFileContent($user_input);
      file_put_contents($form_filename . '.php', $content);
      $content = $this->makeRoutingFileContent($user_input);
      file_put_contents($filename . '.routing.yml', $content);
      $content = $this->makePermissionsFileContent($user_input);
      file_put_contents($filename . '.permissions.yml', $content);
    }
    drupal_set_message($this->t('Your :module_name module has been succesfully created.', array(':module_name' => $user_input['module_name'])));
//    $form_state->setRedirect('skeletons.moduler_created', array('module_name' => $user_input['module_name']));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $machine_name = $user_input['machine_name'];
    if (\Drupal::moduleHandler()->moduleExists($machine_name)) {
      $form_state->setErrorByName('machine_name', $this->t('The selected module already exists!'));
    }
  }

  /**
   * Determines if the machine_name already exists.
   *
   * @param string $id
   *   The action ID
   *
   * @return bool
   *   TRUE if the action exists, FALSE otherwise.
   */
  public function exists($machine_name) {
    $result = \Drupal::moduleHandler()->moduleExists($machine_name);
    return $result;
  }

  private function makeInfoFileContent($user_input) {
    $dependencies = '';
    if (!empty($user_input['module_dependencies'])) {
      $dependencies = "dependencies:";
      $modules = $user_input['module_dependencies'];
      $modules = str_replace(',', ' ',  $modules);
      $modules = str_replace(';', ' ',  $modules);
      while (strpos($modules, '  ') !== FALSE) {
        $modules = str_replace('  ', ' ',  $modules);
      }
      $modules = explode(' ', $modules);
      foreach($modules as $module) {
        $dependencies .= PHP_EOL . "  - $module";
      }
    }
    $package = ucfirst($user_input['module_package']);
    $content = "name: {$user_input['module_name']}"
      . PHP_EOL . "type: module"
      . PHP_EOL . "description: '{$user_input['module_description']}'"
      . PHP_EOL . "package: $package"
//      . PHP_EOL . "version: 8.x-{$user_input['module_version']}"
      . PHP_EOL . "core: 8.x";
    if (!empty($dependencies)) {
      $content .= PHP_EOL . $dependencies;
    }
    return $content . PHP_EOL;
  }

  private function makeInstallFileContent($user_input) {
    $content = '<?php';
    return $content . PHP_EOL;
  }

  private function makeModuleFileContent($user_input) {
    $content = '<?php';
    return $content . PHP_EOL;
  }

  private function makeFormFileContent($user_input) {
    $machine_name = $user_input['machine_name'];
    $classname = $this->makeClassName($machine_name);
    $content = '<?php'
      . PHP_EOL
      . PHP_EOL . '/**'
      . PHP_EOL . '* @file'
      . PHP_EOL . '* Contains \Drupal\\' . $machine_name . '\Form\\' . $classname
      . PHP_EOL . '*/'
      . PHP_EOL
      . PHP_EOL . 'namespace Drupal\\' . $machine_name . '\Form;'
      . PHP_EOL
      . PHP_EOL . 'use Drupal\Core\Form\FormBase;'
      . PHP_EOL . 'use Drupal\Core\Form\FormStateInterface;'
      . PHP_EOL
      . PHP_EOL . 'class ' . $classname . ' extends FormBase {'
      . PHP_EOL
      . PHP_EOL . '  /**'
      . PHP_EOL . '  * Constructs ' . $classname
      . PHP_EOL . '  */'
      . PHP_EOL . '  public function __construct() {'
      . PHP_EOL
      . PHP_EOL . '  }'
      . PHP_EOL
      . PHP_EOL . '  /**'
      . PHP_EOL . '  * {@inheritdoc}'
      . PHP_EOL . '  */'
      . PHP_EOL . '  public function getFormId() {'
      . PHP_EOL . '    return ' . "'" . $machine_name . "_form';"
      . PHP_EOL . '  }'
      . PHP_EOL
      . PHP_EOL . '  /**'
      . PHP_EOL . '  * {@inheritdoc}'
      . PHP_EOL . '  */'
      . PHP_EOL . '  public function buildForm(array $form, FormStateInterface $form_state) {'
      . PHP_EOL . '    return $form;'
      . PHP_EOL . '  }'
      . PHP_EOL
      . PHP_EOL . '  /**'
      . PHP_EOL . '  * {@inheritdoc}'
      . PHP_EOL . '  */'
      . PHP_EOL . '  public function submitForm(array &$form, FormStateInterface $form_state) {'
      . PHP_EOL . '    $user_input = $form_state->getUserInput();'
      . PHP_EOL . '  }'
      . PHP_EOL
      . PHP_EOL . '  /**'
      . PHP_EOL . '  * {@inheritdoc}'
      . PHP_EOL . '  */'
      . PHP_EOL . '  public function validateForm(array &$form, FormStateInterface $form_state) {'
      . PHP_EOL
      . PHP_EOL . '  }'
      . PHP_EOL . '}';
    return $content . PHP_EOL;
  }

  private function makeClassName($machine_name) {
    $name = str_replace('_', ' ', $machine_name);
    $classname = ucwords($name) . 'Form';
    $result = str_replace(' ', '', $classname);

    return $result;
  }

  private function makeRoutingFileContent($user_input) {
    $machine_name = $user_input['machine_name'];
    $classname = $this->makeClassName($machine_name);
    $form_path = ltrim($user_input['form_path'], '/');
    $permission = str_replace('_', ' ', $machine_name);
    $content = "$machine_name.$machine_name:"
      . PHP_EOL . "  path: '/$form_path'"
      . PHP_EOL . "  defaults:"
      . PHP_EOL . "    _form: '\Drupal\\$machine_name\Form\\$classname'"
      . PHP_EOL . "    _title: {$user_input['module_name']}"
      . PHP_EOL . "  requirements:"
      . PHP_EOL . "    _permission: 'view $permission'" ;
    return $content . PHP_EOL;
  }

  private function makePermissionsFileContent($user_input) {
    $machine_name = $user_input['machine_name'];
    $permission = str_replace('_', ' ', $machine_name);
    $form_path = $user_input['form_path'];
    $content = "view $permission:"
      . PHP_EOL . "  title: 'View $permission'"
      . PHP_EOL . "  restrict access: true";
    return $content . PHP_EOL;
  }
}
