<?php

/**
 * @file
 * Contains \Drupal\skeletons\Form\ProfilerForm.
 */

namespace Drupal\skeletons\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Extension\ExtensionDiscovery;

class ProfilerForm extends FormBase {
    /**
   * {@inheritdoc}
   */

  /**
   * Source profile name.
   *
   * @var string
   */
  protected $sourceProfile;
  /**
   * Source profile path.
   * @var string
   */
  protected $sourceProfilePath;
  protected $sourceTheme;
  protected $sourceThemePath;
  protected $destinationProfile;
  protected $destinationProfilePath;
  protected $destinationTheme;
  protected $destinationThemePath;
  protected $profileStorages;

   /**
   * {@inheritdoc}
   */
  public function __construct() {
    $profile = Settings::get('install_profile');
    $profile_storages = [];
    if ($profile) {
      $profile_path = drupal_get_path('module', $profile);
      foreach ([InstallStorage::CONFIG_INSTALL_DIRECTORY, InstallStorage::CONFIG_OPTIONAL_DIRECTORY] as $directory) {
        if (is_dir($profile_path . '/' . $directory)) {
          $profile_storages[] = new FileStorage($profile_path . '/' . $directory, StorageInterface::DEFAULT_COLLECTION);
        }
      }
    }
    $this->profileStorages = $profile_storages;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'skeletons_profiler_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $install_state = NULL) {
    require_once DRUPAL_ROOT . '/core/includes/install.inc';
    // Add list of all available profiles to the installation state.
    $listing = new ExtensionDiscovery(\Drupal::root());
    $listing->setProfileDirectories(array());
    $install_state['profiles'] = $listing->scan('profile');

    // Prime drupal_get_filename()'s static cache.
    foreach ($install_state['profiles'] as $name => $profile) {
       drupal_get_filename('profile', $name, $profile->getPathname());
    }

    $profiles = array();
    $names = array();
    foreach ($install_state['profiles'] as $profile) {
      /** @var $profile \Drupal\Core\Extension\Extension */
      $details = install_profile_info($profile->getName());
      // Don't show hidden profiles. This is used by to hide the testing profile,
      // which only exists to speed up test runs.
      if ($details['hidden'] === TRUE && !drupal_valid_test_ua()) {
        continue;
      }
      $profiles[$profile->getName()] = $details;

      // Determine the name of the profile; default to file name if defined name
      // is unspecified.
      $name = isset($details['name']) ? $details['name'] : $profile->getName();
      $names[$profile->getName()] = $name;
    }

    // Display radio buttons alphabetically by human-readable name, but always
    // put the core profiles first (if they are present in the filesystem).
    natcasesort($names);
    if (isset($names['minimal'])) {
      // If the expert ("Minimal") core profile is present, put it in front of
      // any non-core profiles rather than including it with them alphabetically,
      // since the other profiles might be intended to group together in a
      // particular way.
      $names = array('minimal' => $names['minimal']) + $names;
    }
    if (isset($names['standard'])) {
      // If the default ("Standard") core profile is present, put it at the very
      // top of the list. This profile will have its radio button pre-selected,
      // so we want it to always appear at the top.
      $names = array('standard' => $names['standard']) + $names;
    }

    // The profile name and description are extracted for translation from the
    // .info file, so we can use $this->t() on them even though they are dynamic
    // data at this point.
    $form['source_profile'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Select source installation profile'),
      '#title_display' => 'invisible',
      '#options' => array_map(array($this, 't'), $names),
      '#default_value' => 'standard',
    );
    foreach (array_keys($names) as $profile_name) {
      $form['source_profile'][$profile_name]['#description'] = isset($profiles[$profile_name]['description']) ? $this->t($profiles[$profile_name]['description']) : '';
    }
    $form['destination_profile'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Profile name'),
      '#description' => $this->t('Name of the profile to be created'),
      '#required' => TRUE,
    );
    $dest_profile_paths = array(
      'profiles' => 'profiles',
      'sites/all/profiles' => 'sites/all/profiles',
      'other' => 'Other',
    );
    $form['destination_profile_path'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Select destination profile path'),
      '#title_display' => 'invisible',
      '#options' => $dest_profile_paths,
      '#default_value' => 'profiles',
    );
    $form['destitantion_profile_path_other'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Other destination path'),
      '#description' => $this->t('Provide a destination path.'),
      '#states' => array(
        'visible' => array(
          ':input[name="destination_profile_path"]' => array('value' => 'other'),
        ),
        'required' => array(
          ':input[name="destination_profile_path"]' => array('value' => 'other'),
        ),
      ),
    );
    $form['add_theme'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Add theme'),
      '#default_value' => FALSE,
    );
    $theme_options = array(
      'bartik' => $this->t('Bartik'),
      'classy' => $this->t('Classy'),
      'engines' => $this->t('Engines'),
      'seven' => $this->t('Seven'),
      'stable' => $this->t('Stable'),
      'stark' => $this->t('Stark'),
      );
    $form['source_theme'] = array(
      '#type' => 'radios',
      '#title' => t('Source profile'),
      '#default_value' => 'classy',
      '#options' => $theme_options,
      '#description' => t('Select the source of your theme.'),
      '#states' => array(
        'visible' => array(
          ':input[name="add_theme"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="add_theme"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['destination_theme'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Theme name'),
      '#description' => $this->t('Name of the theme to be created'),
      '#states' => array(
        'visible' => array(
          ':input[name="add_theme"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="add_theme"]' => array('checked' => TRUE),
        ),
      )
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Create'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $this->sourceProfile = $user_input['source_profile'];
    $this->sourceProfilePath = drupal_get_path('profile', $user_input['source_profile']);
    $this->sourceTheme = $user_input['source_theme'];
    $this->sourceThemePath = drupal_get_path('theme', $user_input['source_theme']);
    $this->destinationProfile = $user_input['destination_profile'];

//    $this->destinationProfilePath = $user_input['destination_profile_path'];
    if ($user_input['destination_profile_path'] == 'other') {
      $this->destinationProfilePath = $user_input['destitantion_profile_path_other'];
    }
    else {
      $this->destinationProfilePath = "{$user_input['destination_profile_path']}/{$this->destinationProfile}" ;
    }
    $this->makeDirectories();
    $this->recurseCopy($this->sourceProfilePath, $this->destinationProfilePath);
    if ($user_input['add_theme']) {
      $this->destinationTheme = $user_input['destination_theme'];
      $this->destinationThemePath = $this->destinationProfilePath . '/themes/' . $user_input['destination_theme'];
      $this->recurseCopy($this->sourceThemePath, $this->destinationThemePath);
    }
    drupal_set_message($this->t('Your profile has been succesfully created.'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
  }

  /**
   * Recoursive copy from source to destination.
   *
   * @param type $src
   *   Source of copy.
   * @param type $dst
   *   Destination.
   * @return booelan
   *   TRUE if succeeded otherwise FALSE.
   */
  private function recurseCopy($src, $dst) {
    $dir = opendir($src);
    $result = ($dir === false ? false : true);

    if ($result !== false) {
      if (!file_exists($dst)) {
        @mkdir($dst);
      }
      while(false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..') && $result) {
          if (is_dir($src . '/' . $file)) {
            $result = $this->recurseCopy($src . '/' . $file, $dst . '/' . $file);
          }
          else {
            $result = copy($src . '/' . $file, $dst . '/' . $file);
            $this->saveNewFile($file, $dst, $this->sourceProfile, $this->destinationProfile);
            $this->saveNewFile($file, $dst, $this->sourceTheme, $this->destinationTheme);
          }
        }
      }
      closedir($dir);
    }

    return $result;
  }

  /**
   * Replaces source strings with destination in file $filename.
   *
   * @param string $filename
   *   The filename of processed file.
   */
  private function contentReplace($filename) {
    $content = file_get_contents($filename);
    $content1 = str_replace($this->sourceProfile, $this->destinationProfile, $content);
    $content2 = str_replace(ucfirst($this->sourceProfile), ucfirst($this->destinationProfile), $content1);
    $content3 = str_replace($this->sourceTheme, $this->destinationTheme, $content2);
    $new_content = str_replace(ucfirst($this->sourceTheme), ucfirst($this->destinationTheme), $content3);
    if ($content !== $new_content) {
      file_put_contents($filename, $new_content);
    }
  }

  /**
   * Searches for $source_name and replaces with $destination name in $file and $filename.
   *
   * @param string $file
   *   Original file name.
   * @param string $dst
   *   Destination path.
   * @param atring $source_name
   *   The name to be replaced.
   * @param string $destination_name
   *   The name to replaced with.
   */
  private function saveNewFile($file, $dst, $source_name, $destination_name) {
    $new_file = str_replace($source_name, $destination_name, $file);
    if ($new_file !== $file) {
      $filename = $dst . '/' . $new_file;
      rename($dst . '/' . $file, $filename);
      $this->contentReplace($filename);
    }
  }

  /**
   * Makes directory structure for skeletons.
   */
  private function makeDirectories() {
    \Drupal::service('file_system')->mkdir($this->destinationProfilePath, FileSystem::CHMOD_DIRECTORY, TRUE);
    \Drupal::service('file_system')->mkdir($this->destinationProfilePath . '/config', FileSystem::CHMOD_DIRECTORY, TRUE);
    \Drupal::service('file_system')->mkdir($this->destinationProfilePath . '/config/install', FileSystem::CHMOD_DIRECTORY, TRUE);
    \Drupal::service('file_system')->mkdir($this->destinationProfilePath . '/modules', FileSystem::CHMOD_DIRECTORY, TRUE);
    \Drupal::service('file_system')->mkdir($this->destinationProfilePath . '/modules/contrib', FileSystem::CHMOD_DIRECTORY, TRUE);
    \Drupal::service('file_system')->mkdir($this->destinationProfilePath . '/modules/custom', FileSystem::CHMOD_DIRECTORY, TRUE);
    \Drupal::service('file_system')->mkdir($this->destinationProfilePath . '/src', FileSystem::CHMOD_DIRECTORY, TRUE);
    if (!empty($this->destinationThemePath)) {
      \Drupal::service('file_system')->mkdir($this->destinationProfilePath . '/themes', FileSystem::CHMOD_DIRECTORY, TRUE);
      \Drupal::service('file_system')->mkdir($this->destinationThemePath, FileSystem::CHMOD_DIRECTORY, TRUE);
    }
  }
}