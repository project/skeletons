<?php

/**
 * @file
 * Contains \Drupal\skeletons\Form\SkeletonsForm.
 */

namespace Drupal\skeletons\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SkeletonsForm extends FormBase {
    /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'skeletons_skeletons_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = array(
      '#markup' => '<p>' . $this->t('To create a module skeleton select Moduler tab.') . '</p>'
      . '<p>' . $this->t('To create an install profile skeleton select Profiler tab.') . '</p>'
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
//    $form_state->setRedirect('config_partial.export_partial_download');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    return TRUE;
  }
}